﻿﻿using System.Threading;
using System.Threading.Tasks;
using Consul;

namespace Cloudburst.ServiceRegistration.Repositories.Abstractions {
	public interface IConsulRepository : IServiceRegister<AgentServiceRegistration> {
		Task RegisterWithConsulAsync(AgentServiceRegistration agentServiceRegistration, CancellationToken cancellationToken = default(CancellationToken));
		Task UnregisterWithConsulAsync(AgentServiceRegistration agentServiceRegistration, CancellationToken cancellationToken = default(CancellationToken));
		Task UnregisterWithConsulAsync(string registrationId, CancellationToken cancellationToken = default(CancellationToken));
		AgentServiceRegistration GetAgentServiceRegistration();
	}
}
