using System.Threading;
using System.Threading.Tasks;

namespace Cloudburst.ServiceRegistration.Repositories.Abstractions {
	public interface IServiceRegister<TRegister> {
		Task<TRegister> RegisterServiceAsync(TRegister serviceRegistration, CancellationToken cancellationToken = default(CancellationToken));
		Task UnregisterServiceAsync(TRegister serviceRegistration, CancellationToken cancellationToken = default(CancellationToken));
	}
}
