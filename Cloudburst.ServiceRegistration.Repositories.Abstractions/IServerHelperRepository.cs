using System.Net;

namespace Cloudburst.ServiceRegistration.Repositories.Abstractions {
	public interface IServerHelperRepository {
		IPAddress GetIpAddress();
		int GetPort();
		string GetScheme();
	}
}
