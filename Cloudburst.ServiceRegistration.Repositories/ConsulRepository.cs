﻿﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Cloudburst.ServiceRegistration.Abstractions;
using Cloudburst.ServiceRegistration.Models;
using Cloudburst.ServiceRegistration.Repositories.Abstractions;
using Cloudburst.Version.Abstractions;
using Consul;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Polly;
using Polly.CircuitBreaker;
using Polly.Retry;
using Polly.Wrap;

namespace Cloudburst.ServiceRegistration.Repositories {
	public class ConsulRepository : IConsulRepository {
		private readonly ILogger<ConsulRepository> _logger;
		private readonly IConsulClient _consulClient;
		private readonly IOptionsMonitor<ServiceDiscoveryOptions> _options;
		private readonly IServiceVersion _serviceVersion;
		private readonly IServerHelperRepository _serverHelperRepository;
		private readonly IHostEnvironment _env;

		private ServiceDiscoveryOptions _config => _options.CurrentValue;

		private AsyncRetryPolicy _retryPolicy;
		private AsyncCircuitBreakerPolicy _circuitBreakerPolicy;
		private AsyncPolicyWrap _policy;

		public ConsulRepository(ILogger<ConsulRepository> logger, IConsulClient consulClient, IOptionsMonitor<ServiceDiscoveryOptions> options, IServiceVersion serviceVersion, IServerHelperRepository serverHelperRepository, IHostEnvironment env) {
			_logger = logger;
			_consulClient = consulClient;
			_options = options;
			_serviceVersion = serviceVersion;
			_serverHelperRepository = serverHelperRepository;
			_env = env;

			int retryCount = 1;

			_retryPolicy = Policy
				.Handle<HttpRequestException>()
				.Or<OperationCanceledException>()
				.WaitAndRetryAsync(
					retryCount,
					retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt)),
					(exception, timeSpan, count, context) => {
						_logger.LogWarning($"Unable to register service with Consul. Retrying in {timeSpan.TotalSeconds} seconds. {retryCount - (count - 1)} attempts remaining.");
						_logger.LogDebug(exception, exception.Message);
					});

			_circuitBreakerPolicy = Policy
				.Handle<HttpRequestException>()
				.Or<OperationCanceledException>()
				.CircuitBreakerAsync(
					retryCount,
					TimeSpan.FromSeconds(3),
					(exception, timeSpan) => {
						_logger.LogWarning($"Breaking circuit for Consul for {timeSpan.TotalSeconds} seconds.");
						_logger.LogDebug(exception, exception.Message);
					},
					() => _logger.LogInformation("Consul circuit breaker reset."),
					() => _logger.LogWarning("Consul circuit breaker is half-open.")
				);

			_policy = Policy.WrapAsync(_retryPolicy, _circuitBreakerPolicy);
		}

		public async Task<AgentServiceRegistration> RegisterServiceAsync(AgentServiceRegistration serviceRegistration, CancellationToken cancellationToken = default(CancellationToken)) {
			await RegisterWithConsulAsync(serviceRegistration, cancellationToken);
			return await Task.FromResult(serviceRegistration);
		}

		public async Task UnregisterServiceAsync(AgentServiceRegistration serviceRegistration, CancellationToken cancellationToken = default(CancellationToken)) {
			await UnregisterWithConsulAsync(serviceRegistration, cancellationToken);
		}

		public async Task RegisterWithConsulAsync(AgentServiceRegistration agentServiceRegistration, CancellationToken cancellationToken = default(CancellationToken)) {
			try {
				await _retryPolicy.ExecuteAsync(
					async token => {
						var result = _consulClient.Agent.ServiceRegister(agentServiceRegistration, token);
						WriteResult realResult = await result;
						_logger.LogInformation($"Register Result : {realResult.StatusCode.ToString()}");
						return result;
					},
					cancellationToken
				);
			} catch (Exception exception) {
				throw new ServiceRegistrationException(exception.Message);
			}
		}

		public async Task UnregisterWithConsulAsync(AgentServiceRegistration agentServiceRegistration, CancellationToken cancellationToken = default(CancellationToken)) {
			await UnregisterWithConsulAsync(agentServiceRegistration.ID, cancellationToken);
		}

		public async Task UnregisterWithConsulAsync(string registrationId, CancellationToken cancellationToken = default(CancellationToken)) {
			try {
				await _retryPolicy.ExecuteAsync(
					token => _consulClient.Agent.ServiceDeregister(registrationId, token),
					cancellationToken
				);
			} catch (Exception exception) {
				throw new ServiceRegistrationException(exception.Message);
			}
		}

		public AgentServiceRegistration GetAgentServiceRegistration() {
			if (String.IsNullOrEmpty(_config.ServiceAddress)) {
				_config.ServiceAddress = _serverHelperRepository.GetIpAddress()?.ToString();

				// Still no Service Address? Service Registration isn't going to work...
				if (String.IsNullOrEmpty(_config.ServiceAddress)) {
					throw new ServiceRegistrationException("Could not parse Service Address so registration is stopped.");
				}
			}

			if (_config.ServicePort == 0) {
				_config.ServicePort = _serverHelperRepository.GetPort();

				// Still no Service Port? Service Registration isn't going to work...
				if (_config.ServicePort == 0) {
					throw new ServiceRegistrationException("Could not parse Service Port so registration is stopped.");
				}
			}

			if (String.IsNullOrEmpty(_config.HealthCheckBase)) {
				string scheme = _serverHelperRepository.GetScheme();
				_config.HealthCheckBase = $"{scheme}://{_config.ServiceAddress}:{_config.ServicePort}";
			}

			var healthCheck = new UriBuilder(_config.HealthCheckBase);
			healthCheck.Path = !String.IsNullOrEmpty(_config.HealthCheckPath) ? _config.HealthCheckPath : "/health";

			if (_config.UseSimpleHealthCheck) {
				// We add the UID to the health request url because if a new service replaces an old service on the
				// same IP Address & Port, we don't want to "take over" that service because we could be a completely
				// different API, have a different version, etc.
				// By making the URI dynamic to the registered service, the old, no-longer-valid service will drop out
				// of the registration server when the health checks fail.
				healthCheck.Path += $"/{_serviceVersion.UniqueInstanceIdentifier}";
			}

			var tags = new List<string>();
			if (!(_config.ServiceTags is null)) {
				tags.AddRange(_config.ServiceTags);
			}
			tags.AddRange(new[] {
				_config.ServiceName,
				$"{_config.ServiceName} {_serviceVersion}",
				_serviceVersion.UniqueInstanceIdentifier.ToString(),
				_env.EnvironmentName
			});

			return new AgentServiceRegistration {
				ID = $"{_config.ServiceName} {_serviceVersion} ({_serviceVersion.UniqueInstanceIdentifier})",
				Name = $"{_config.ServiceName} {_serviceVersion}",
				Address = _config.ServiceAddress,
				Port = _config.ServicePort,
				Tags = tags.ToArray(),
				// TODO: Allow multiple AgentServiceChecks
				Check = new AgentServiceCheck {
					HTTP = healthCheck.ToString(),
					Timeout = TimeSpan.FromSeconds(_config.HealthCheckTimeout),
					Interval = TimeSpan.FromSeconds(_config.HealthCheckInterval),
					TLSSkipVerify = !_config.HealthCheckTlsVerify,
					DeregisterCriticalServiceAfter = TimeSpan.FromSeconds(_config.DeregisterCriticalServiceAfter)
					//TTL = TimeSpan.FromSeconds(_config.HealthCheckInterval)
				}
			};
		}

	}
}
