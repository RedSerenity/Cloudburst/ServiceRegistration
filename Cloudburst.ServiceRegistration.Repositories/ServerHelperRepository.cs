using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using Cloudburst.ServiceRegistration.Repositories.Abstractions;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Hosting.Server.Features;
using Microsoft.Extensions.Logging;

namespace Cloudburst.ServiceRegistration.Repositories {
	public class ServerHelperRepository : IServerHelperRepository {
		private readonly ILogger<ServerHelperRepository> _logger;
		private readonly IServer _server;

		public ServerHelperRepository(ILogger<ServerHelperRepository> logger, IServer server) {
			_logger = logger;
			_server = server;
		}

		public IPAddress GetIpAddress() {
			string hostName = Dns.GetHostName();
			try {
				IPAddress containerIpAddress = Dns.GetHostEntry(hostName)
						.AddressList.FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork);
				return containerIpAddress;
			} catch (SocketException exception) {
				_logger.LogWarning(exception, "Could not find Container Ip Address.");
				return null;
			}
		}

		public int GetPort() {
			string ipAddress = _server.Features?.Get<IServerAddressesFeature>()?.Addresses.FirstOrDefault();
			return String.IsNullOrEmpty(ipAddress) ? 0 : new Uri(ipAddress.Replace("*", "localhost")).Port;
		}

		public string GetScheme() {
			string ipAddress = _server.Features?.Get<IServerAddressesFeature>()?.Addresses.FirstOrDefault();
			return String.IsNullOrEmpty(ipAddress) ? null : new Uri(ipAddress.Replace("*", "localhost")).Scheme;
		}
	}
}
