﻿using System.ComponentModel.DataAnnotations;
using Cloudburst.Configuration.Attributes;
//TODO: HealthCheckPort
//TODO: Separate Http port from Grpc Port - ie, multiple services
//TODO: Add service name and version verification to the health check
//TODO: Add Extension to register Grpc Client that automatically finds client address from Consul
namespace Cloudburst.ServiceRegistration.Models {
	[ConfigurationKey("Cloudburst:ServiceDiscovery")]
	public class ServiceDiscoveryOptions {
		public const int DEFAULT_SERVICE_REGISTRATION = 60;

		public string ServiceDiscoveryAddress { get; set; } = "http://localhost:8500";
		public string ServiceDiscoveryDataCenter { get; set; } = "dc1";
		[Required]
		public string ServiceName { get; set; }
		public string[] ServiceTags { get; set; }
		public string ServiceAddress { get; set; }
		public int ServicePort { get; set; } = 0;
		public int DeregisterCriticalServiceAfter { get; set; } = 60;
		public bool UseSimpleHealthCheck { get; set; } = false;
		public string HealthCheckBase { get; set; }
		public string HealthCheckPath { get; set; }
		public int HealthCheckTimeout { get; set; } = 3;
		public int HealthCheckInterval { get; set; } = 30;
		public bool HealthCheckTlsVerify { get; set; } = false;
		public int UpdateFrequency { get; set; } = DEFAULT_SERVICE_REGISTRATION;
	}
}
