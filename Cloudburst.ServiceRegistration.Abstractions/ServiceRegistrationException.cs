using System;

namespace Cloudburst.ServiceRegistration.Abstractions {
	public class ServiceRegistrationException : Exception {
		public ServiceRegistrationException(string message) : base(message) { }
	}
}
