using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Cloudburst.ServiceRegistration.Abstractions {
	public interface IRegistryServices {
		Task<IList<Uri>> GetByTagsAsync(string[] tags);
		Task<IList<Uri>> GetByNameAsync(string serviceName);
		Task<IList<Uri>> GetByIdAsync(string serviceId);
	}
}
