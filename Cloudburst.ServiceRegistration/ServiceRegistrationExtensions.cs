﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cloudburst.Configuration;
using Cloudburst.ServiceRegistration.Models;
using Cloudburst.ServiceRegistration.Repositories;
using Cloudburst.ServiceRegistration.Repositories.Abstractions;
using Cloudburst.ServiceRegistration.Workers;
using Cloudburst.Version.Abstractions;
using Consul;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;

namespace Cloudburst.ServiceRegistration {
	public static class ServiceRegistrationExtensions {
		public static IHostBuilder UseCloudburstServiceRegistration(this IHostBuilder builder, Action<ConsulClientConfiguration, ServiceDiscoveryOptions> consulClientConfiguration = null) {
			builder.ConfigureServices((buildContext, services) => {
				services.AddServiceRegistration(buildContext.Configuration, consulClientConfiguration);
			});

			return builder;
		}

		public static IServiceCollection AddServiceRegistration(this IServiceCollection services, IConfiguration config, Action<ConsulClientConfiguration, ServiceDiscoveryOptions> consulClientConfiguration = null) {
			services.Configure<HostOptions>(x => { x.ShutdownTimeout = TimeSpan.FromSeconds(20); });

			IOptionsMonitor<ServiceDiscoveryOptions> serviceDiscoveryOptions;

			try {
				serviceDiscoveryOptions = services.ConfigureMonitor<ServiceDiscoveryOptions>(config);

				// Check if there was an exception so that it triggers here and not later.
				if (serviceDiscoveryOptions.CurrentValue is null) { }
			} catch (OptionsValidationException exception) {
				Console.WriteLine("ServiceName is missing from the ServiceDiscovery options. Service Discovery Registration will be skipped.");
				return services;
			}

			if (consulClientConfiguration is null) {
				consulClientConfiguration = (consulConfig, options) => {
					consulConfig.Address = new Uri(options.ServiceDiscoveryAddress);
					consulConfig.Datacenter = options.ServiceDiscoveryDataCenter;
				};
			}

			// TODO: Add Polly
			services.TryAddSingleton<IConsulClient>(x => {
				return new ConsulClient(consulConfig => { consulClientConfiguration.Invoke(consulConfig, serviceDiscoveryOptions.CurrentValue); });

				// This doesn't work the way I had hoped. I need a way to recreate this instance when the configuration changes...
				IConsulClient instance = new ConsulClient(consulConfig => { consulClientConfiguration.Invoke(consulConfig, serviceDiscoveryOptions.CurrentValue); });

				var options = x.GetService<IOptionsMonitor<ServiceDiscoveryOptions>>();
				options.OnChange(discoveryOptions => instance = new ConsulClient(consulConfig => { consulClientConfiguration.Invoke(consulConfig, serviceDiscoveryOptions.CurrentValue); }));

				return instance;
			});

			services.AddScoped<IServerHelperRepository, ServerHelperRepository>();
			services.AddScoped<IConsulRepository, ConsulRepository>();

			services.AddHostedService<ServiceRegistrationWorker>();

			if (serviceDiscoveryOptions.CurrentValue.UseSimpleHealthCheck) {
				services.AddHealthChecks()
					.AddCheck("SimpleHealthCheck", () => HealthCheckResult.Healthy());
			}

			return services;
		}

		public static IApplicationBuilder UseServiceRegistration(this IApplicationBuilder builder) {
			IOptionsMonitor<ServiceDiscoveryOptions> options;
			try {
				options = builder.ApplicationServices.GetService<IOptionsMonitor<ServiceDiscoveryOptions>>();
				// Check if there was an exception so that it triggers here and not later.
				if (options.CurrentValue is null) { }
			} catch (OptionsValidationException exception) {
				return builder;
			}

			if (options.CurrentValue.UseSimpleHealthCheck) {
				var version = builder.ApplicationServices.GetService<IServiceVersion>();
				builder.UseHealthChecks($"/health/{version.UniqueInstanceIdentifier}");
			}

			return builder;
		}

		public static bool ContainsTags(this string[] source, string[] subset) {
			var hashSet = new HashSet<string>(subset.AsEnumerable(), EqualityComparer<string>.Default);
			if (hashSet.Count == 0) {
				return true;
			}

			foreach (var item in source) {
				hashSet.Remove(item);
				if (hashSet.Count == 0) {
					break;
				}
			}

			return hashSet.Count == 0;
		}
	}
}
