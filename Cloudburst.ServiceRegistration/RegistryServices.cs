using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Cloudburst.ServiceRegistration.Abstractions;
using Consul;

namespace Cloudburst.ServiceRegistration {
	// TODO: Add Polly
	// TODO: Load all services as IOptionMonitor<> types for easier access?
	public class RegistryServices : IRegistryServices {
		private readonly IConsulClient _consulClient;

		public RegistryServices(IConsulClient consulClient) {
			_consulClient = consulClient;
		}

		private async Task<Dictionary<string, AgentService>> QueryServicesAsync() {
			var queryResult = await _consulClient.Agent.Services();
			return queryResult.Response;
		}

		private async Task<IList<Uri>> _GetByHelper(Func<AgentService, bool> comparer) {
			if (comparer is null) {
				throw new ArgumentNullException(nameof(comparer));
			}

			var services = await QueryServicesAsync();
			var servicesUrls = new List<Uri>();

			foreach (var service in services) {
				AgentService agent = service.Value;

				if (comparer(agent)) {
					servicesUrls.Add(new Uri($"{agent.Address}:{agent.Port}"));
				}
			}

			return servicesUrls;
		}

		public async Task<IList<Uri>> GetByTagsAsync(string[] tags) => await _GetByHelper(service => service.Tags.ContainsTags(tags));
		public async Task<IList<Uri>> GetByNameAsync(string serviceName) => await _GetByHelper(service => service.Service.Equals(serviceName));
		public async Task<IList<Uri>> GetByIdAsync(string serviceId) => await _GetByHelper(service => service.ID.Equals(serviceId));
	}
}

