using System;
using System.Threading;
using System.Threading.Tasks;
using Cloudburst.ServiceRegistration.Abstractions;
using Cloudburst.ServiceRegistration.Models;
using Cloudburst.ServiceRegistration.Repositories.Abstractions;
using Consul;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace Cloudburst.ServiceRegistration.Workers {
	public class ServiceRegistrationWorker : IHostedService, IDisposable {
		private Task _executingTask;
		private CancellationTokenSource _cancellationTokenSource;

		private readonly ILogger<ServiceRegistrationWorker> _logger;
		private readonly IServiceScopeFactory _serviceScopeFactory;
		private readonly IOptionsMonitor<ServiceDiscoveryOptions> _options;
		private readonly IHostApplicationLifetime _applicationLifetime;

		private AgentServiceRegistration _agentServiceRegistration;

		//TODO: Eventually change IConsulRepository to a Visitor/Factory pattern to allow a config option to determine
		//TODO: which repository to use for ServiceDiscovery Registration
		public ServiceRegistrationWorker(ILogger<ServiceRegistrationWorker> logger, IServiceScopeFactory serviceScopeFactory, IOptionsMonitor<ServiceDiscoveryOptions> options, IHostApplicationLifetime applicationLifetime) {
			_logger = logger;
			_serviceScopeFactory = serviceScopeFactory;
			_options = options;
			_applicationLifetime = applicationLifetime;
		}

		public async Task StartAsync(CancellationToken cancellationToken) {
			_cancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);

			// We need to wait until Kestrel has been started in order to
			// to automatically determine the IP Address to use for service
			// registration - assuming it hasn't been specified in the config.
			_applicationLifetime.ApplicationStarted.Register(async () => {
				_logger.LogInformation("Application has started. Beginning Service Discovery Registration.");
				_executingTask = ExecuteAsync(_cancellationTokenSource.Token);

				if (_executingTask.IsCompleted) {
					await _executingTask;
				}
			});

			_applicationLifetime.ApplicationStopping.Register(async () => {
				using (var scope = _serviceScopeFactory.CreateScope()) {
					_logger.LogInformation("Application is stopping. Unregistering service from Service Discovery.");
					IConsulRepository consulRepository = scope.ServiceProvider.GetRequiredService<IConsulRepository>();
					await consulRepository.UnregisterServiceAsync(_agentServiceRegistration);
				}
			});

			await Task.CompletedTask;
		}

		public async Task StopAsync(CancellationToken cancellationToken) {
			if (_executingTask == null) {
				return;
			}

			try {
				_cancellationTokenSource.Cancel();
			} finally {
				await Task.WhenAny(_executingTask, Task.Delay(Timeout.Infinite, cancellationToken));
			}
		}

		private async Task ExecuteAsync(CancellationToken cancellationToken) {
			using (var scope = _serviceScopeFactory.CreateScope()) {
				IConsulRepository consulRepository = scope.ServiceProvider.GetRequiredService<IConsulRepository>();

				while (!cancellationToken.IsCancellationRequested) {
					try {
						_agentServiceRegistration = consulRepository.GetAgentServiceRegistration();

						_logger.LogInformation($"Registering service {_agentServiceRegistration.ID}");
						await consulRepository.RegisterServiceAsync(_agentServiceRegistration, cancellationToken);
					} catch (ServiceRegistrationException exception) {
						_logger.LogWarning($"Could not register service {_agentServiceRegistration.ID}. A registration exception was thrown.");
						_logger.LogDebug(exception, exception.Message);
					} catch (Exception exception) {
						// Catch any unhandled exceptions and log them so that the Background Service doesn't die.
						_logger.LogError(exception, exception.Message);
					}

					int updateFrequency = _options.CurrentValue.UpdateFrequency;
					// Only allow an update every 60 seconds at the most. Anything less than that,
					// default to DEFAULT_SERVICE_REGISTRATION.
					// The real question, is how long do we need to do this? Or will Consul's
					// health check make this unnecessary?
					// But what if network connectivity is lost, and Consul purges itself? Then
					// we will need to re-register....
					if (updateFrequency < 60) {
						updateFrequency = ServiceDiscoveryOptions.DEFAULT_SERVICE_REGISTRATION;
					}
					// Update the registration every X seconds.
					await Task.Delay(TimeSpan.FromSeconds(updateFrequency), cancellationToken);
				}

				if (_agentServiceRegistration is null) {
					return;
				}

				// Don't pass in the cancellation token here because we are already in the shutdown phase
				// and it would immediately cancel the unregister request. :-(
				await consulRepository.UnregisterServiceAsync(_agentServiceRegistration);
			}
		}

		public void Dispose() {
			_cancellationTokenSource.Cancel();
		}
	}
}
